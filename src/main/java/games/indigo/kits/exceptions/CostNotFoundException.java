package games.indigo.kits.exceptions;

public class CostNotFoundException extends Exception {

    public CostNotFoundException(String error) {
        super(error);
    }

}
