package games.indigo.kits.exceptions;

public class KitNotFoundException extends Exception {

    public KitNotFoundException(String error) {
        super(error);
    }

}
