package games.indigo.kits;

import games.indigo.kits.costs.Cost;
import net.darkscorner.darkscooldown.Cooldown;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Kit {

    private String identifier, name;
    private Material icon;
    private Cost cost;
    private int cooldown, costValue;
    private List<ItemStack> items;

    /**
     * Constructor for kits
     * @param identifier
     * @param name
     * @param items
     */
    public Kit(String identifier, String name, Material icon, Cost cost, int costValue, int cooldown, List<ItemStack> items) {
        this.identifier = identifier;
        this.name = name;
        this.icon = icon;
        this.cost = cost;
        this.costValue = costValue;
        this.cooldown = cooldown;
        this.items = items;
    }

    /**
     * Get the identifier of the kit
     * @return The identifier of the kit
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Get the name of the kit
     * @return The name of the kit
     */
    public String getName() {
        return StringUtils.capitalize(getIdentifier());
    }

    /**
     * Get the icon material value for the kit
     * @return The icon material value for the kit
     */
    public Material getIcon() { return icon; }

    /**
     * Get the cost type of the kit
     * @return The cost type of the kit
     */
    public Cost getCost() { return cost; }

    /**
     * Get the cost value of the kit
     * @return The cost value of the kit
     */
    public int getCostValue() { return costValue; }

    /**
     * Get the cooldown length in seconds for the kit
     * @return The cooldown length in seconds for the kit
     */
    public int getCooldown() { return cooldown; }

    /**
     * Get the items in the kit
     * @return The items in the kit
     */
    public List<ItemStack> getItems() {
        return items;
    }

    /**
     * Give kit to player
     * @param player The player to give the kit to
     */
    public void giveKit(Player player) {
        Cooldown kitCooldown = Cooldown.getCooldown(player, "kits" + getName());
        if (kitCooldown == null || kitCooldown.isExpired()) {
            new Cooldown(player, "kits" + getName(), getCooldown());
            player.sendMessage(Kits.getInstance().getUtils().format("&2&l(!) &aClaimed kit: &f" + getName()));
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.5f, 1);
            for (ItemStack item : getItems()) {
                player.getInventory().addItem(item);
            }
        } else {
            player.sendMessage(Kits.getInstance().getUtils().format("&4&l(!) &cThis kit is still on cooldown! &f" + kitCooldown.getFormattedTimeLeft() + " &cremaining!"));
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 0.5f, 1);
        }
    }
}
