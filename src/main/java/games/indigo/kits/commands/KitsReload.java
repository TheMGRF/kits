package games.indigo.kits.commands;

import games.indigo.kits.Kits;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class KitsReload implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Kits.getInstance().reloadKits();
        sender.sendMessage(Kits.getInstance().getUtils().format("&6&l(!) &eReloaded kits config!"));
        return true;
    }
}
