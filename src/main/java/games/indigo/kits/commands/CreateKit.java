package games.indigo.kits.commands;

import games.indigo.kits.Kits;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateKit implements CommandExecutor {

    private Kits kits = Kits.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("indigo.kits.create")) {
                if (player.getInventory().getItemInMainHand().getType() != null && player.getInventory().getItemInMainHand().getType() != Material.AIR) {
                    if (args.length > 0) {
                        if (args.length > 1) {
                            Kits.getInstance().getKitCreatorGUI().open(player, args[0], args[1], Integer.parseInt(args[2]), player.getInventory().getItemInMainHand().getType().name());
                        } else {
                            player.sendMessage(kits.getUtils().format("&cPlease follow this format &7/createkit <category> <name> <cooldown>"));
                        }
                    } else {
                        player.sendMessage(kits.getUtils().format("&cPlease follow this format &7/createkit <category> <name> <cooldown>"));
                    }
                } else {
                    player.sendMessage(kits.getUtils().format("&cPlease hold the kit icon in your hand!"));
                }
            } else {
                player.sendMessage(kits.getUtils().format("&cYou do not have access to this command!"));
            }
        }
        return true;
    }
}
