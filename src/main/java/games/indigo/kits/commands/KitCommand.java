package games.indigo.kits.commands;

import games.indigo.kits.Kit;
import games.indigo.kits.Kits;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KitCommand implements CommandExecutor {

    private Kits main = Kits.getInstance();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                main.getKitCategoryGUI().open(player);
            } else {
                // TODO: Check to see if kit exists
                findKit(player, args[0]);
            }
        }
        return true;
    }

    public void findKit(Player player, String id) {
        for (Kit kit : main.getKits()) {
            if (kit.getIdentifier().equals(id)) {
                kit.giveKit(player);
                return;
            }
        }
        player.sendMessage(main.getUtils().format("&4&l(!) &cCould not find a kit by that name!"));
    }

}
