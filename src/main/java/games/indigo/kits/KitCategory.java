package games.indigo.kits;

import org.bukkit.Material;

import java.util.List;

public class KitCategory {

    private String identifier, name, description;
    private Material icon;
    private int slot;
    private List<Kit> kits;

    /**
     * Constructor for creating a kit category
     * @param identifier
     * @param name
     * @param description
     * @param icon
     * @param slot
     */
    public KitCategory(String identifier, String name, String description, Material icon, int slot, List<Kit> kits) {
        this.identifier = identifier;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.slot = slot;
        this.kits = kits;
    }

    /**
     *
     * @return
     */
    public String getIdentifier() { return identifier; }

    /**
     *
     * @return
     */
    public String getName() { return name; }

    /**
     *
     * @return
     */
    public String getDescription() { return description; }

    /**
     *
     * @return
     */
    public Material getIcon() { return icon; }

    /**
     *
     * @return
     */
    public int getSlot() { return slot; }

    /**
     *
     * @return
     */
    public List<Kit> getKits() { return kits; }
}
