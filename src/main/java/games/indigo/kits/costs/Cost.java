package games.indigo.kits.costs;

public abstract class Cost {

    private String identifier, colours, symbol;

    /**
     *
     * @param identifier
     * @param colours
     * @param symbol
     */
    public Cost(String identifier, String colours, String symbol) {
        this.identifier = identifier;
        this.colours = colours;
        this.symbol = symbol;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getColours() {
        return colours;
    }

    public String getSymbol() {
        return symbol;
    }
}
