package games.indigo.kits.costs;

public class CostFree extends Cost {

    public CostFree() {
        super("free", "&d&l", "FREE");
    }
}
