package games.indigo.kits.costs;

public class CostCash extends Cost {

    public CostCash() {
        super("cash", "&a&l", "$");
    }

}
