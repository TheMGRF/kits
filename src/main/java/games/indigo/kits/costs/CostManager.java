package games.indigo.kits.costs;

import games.indigo.kits.exceptions.CostNotFoundException;

import java.util.Arrays;
import java.util.List;

public class CostManager {

    /**
     * Get a cost object by its ID
     * @param id
     * @return
     * @throws CostNotFoundException
     */
    public Cost getCostById(String id) throws CostNotFoundException {
        List<Cost> costs = Arrays.asList(new CostFree(), new CostCash(), new CostCrystals());
        for (Cost cost : costs) {
            if (id.equals(cost.getIdentifier())) {
                return cost;
            }
        }
        throw new CostNotFoundException("Incorrect cost value: \"" + id + "\"");
    }

}
