package games.indigo.kits;

import games.indigo.kits.commands.CreateKit;
import games.indigo.kits.commands.KitCommand;
import games.indigo.kits.commands.KitsReload;
import games.indigo.kits.costs.Cost;
import games.indigo.kits.costs.CostManager;
import games.indigo.kits.guis.KitCategoryGUI;
import games.indigo.kits.guis.KitCreatorGUI;
import games.indigo.kits.guis.KitGUI;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Kits extends JavaPlugin {

    private static Kits instance;

    private Utils utils;
    private KitCategoryGUI kitCategoryGUI;
    private KitGUI kitGUI;
    private KitCreatorGUI kitCreatorGUI;

    private List<KitCategory> kitCategories;
    private List<Kit> kits;

    // Config
    public File configFile;
    public FileConfiguration config;

    public void onEnable() {
        instance = this;

        reloadKits();

        utils = new Utils();
        kitCategoryGUI = new KitCategoryGUI();
        kitGUI = new KitGUI();
        kitCreatorGUI = new KitCreatorGUI();

        getCommand("kit").setExecutor(new KitCommand());
        getCommand("createkit").setExecutor(new CreateKit());
        getCommand("kitsreload").setExecutor(new KitsReload());
    }

    public static Kits getInstance() {
        return instance;
    }

    public void reloadKits() {
        loadConfig();

        kitCategories = new ArrayList<>();
        if (config.isSet("kits")) {
            for (String category : config.getConfigurationSection("kits").getKeys(false)) {
                try {
                    String categoryName = config.getString("kits." + category + ".name");
                    String categoryDesc = config.getString("kits." + category + ".description");
                    Material categoryIcon = Material.valueOf(config.getString("kits." + category + ".icon"));
                    int categorySlot = config.getInt("kits." + category + ".slot");
                    List<Kit> categoryKits = new ArrayList<>();

                    for (String kitID : config.getConfigurationSection("kits." + category + ".kits").getKeys(false)) {
                        String kitName = config.getString("kits." + category + ".kits." + kitID + ".name");
                        int cooldown = config.getInt("kits." + category + ".kits." + kitID + ".cooldown");
                        Material kitIcon = Material.valueOf(config.getString("kits." + category + ".kits." + kitID + ".icon"));
                        Cost cost = new CostManager().getCostById(config.getString("kits." + category + ".kits." + kitID + ".costType"));
                        int costValue = config.getInt("kits." + category + ".kits." + kitID + ".costValue");
                        List<ItemStack> kitItems = new ArrayList<>();
                        if (config.getList("kits." + category + ".kits." + kitID + ".items") != null) {
                            for (Object kitItem : config.getList("kits." + category + ".kits." + kitID + ".items")) {
                                kitItems.add((ItemStack) kitItem);
                            }
                        }
                        categoryKits.add(new Kit(kitID, kitName, kitIcon, cost, costValue, cooldown, kitItems));
                    }

                    kitCategories.add(new KitCategory(category, categoryName, categoryDesc, categoryIcon, categorySlot, categoryKits));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        kits = new ArrayList<>();
        for (KitCategory kitCategory : kitCategories) {
            for (Kit kit : kitCategory.getKits()) {
                kits.add(kit);
            }
        }
    }

    public void loadConfig() {
        // Custom Config
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            saveResource("config.yml", false);
        }

        config = new YamlConfiguration();
        try {
            config.load(configFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public FileConfiguration getConfig() {
        return config;
    }

    public File getConfigFile() {
        return configFile;
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Utils getUtils() {
        return utils;
    }

    public KitCategoryGUI getKitCategoryGUI() {
        return kitCategoryGUI;
    }

    public KitGUI getKitGUI() {
        return kitGUI;
    }

    public KitCreatorGUI getKitCreatorGUI() {
        return kitCreatorGUI;
    }

    public List<Kit> getKits() {
        return kits;
    }

    public List<KitCategory> getKitCategories() {
        return kitCategories;
    }
}
