package games.indigo.kits.guis;

import com.asylumdevs.inventoryapi.Inventory;
import com.asylumdevs.inventoryapi.InventoryAPI;
import com.asylumdevs.inventoryapi.ItemListener;
import games.indigo.kits.Kits;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class KitCreatorGUI {

    private Kits kits = Kits.getInstance();

    public void open(Player player, String category, String kitName, int cooldown, String icon) {
        Inventory inv = InventoryAPI.createInventory1(kits.getUtils().format("&6&lKit Creator &8&l> &8" + kitName), 54);

        inv.setSlot(45, InventoryAPI.createItem(kits.getUtils().format("&c&lCancel"), null, Material.BARRIER, null, 1), new ItemListener() {
            @Override
            public void onItemClick(Player player, ClickType clickType) {
                for (ItemStack item : getKitItems(inv)) {
                    player.getInventory().addItem(item);
                }

                player.closeInventory();
                player.sendMessage(kits.getUtils().format("&4&l(!) &cKit Cancelled!"));
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
            }
        });

        inv.setSlot(53, InventoryAPI.createItem(kits.getUtils().format("&a&lCreate"), null, Material.EMERALD, null, 1), new ItemListener() {
            @Override
            public void onItemClick(Player player, ClickType clickType) {
                kits.getConfig().set("kits." + category + ".kits." + kitName + ".cooldown", cooldown);
                kits.getConfig().set("kits." + category + ".kits." + kitName + ".costType", "free");
                kits.getConfig().set("kits." + category + ".kits." + kitName + ".costValue", 0);
                kits.getConfig().set("kits." + category + ".kits." + kitName + ".icon", icon);
                kits.getConfig().set("kits." + category + ".kits." + kitName + ".items", getKitItems(inv));

                kits.saveConfig();
                kits.reloadKits();

                player.closeInventory();
                player.sendMessage(kits.getUtils().format("&2&l(!) &aKit \"&f" + kitName + "&a\" Created!"));
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
            }
        });

        inv.open(player);
    }

    private List<ItemStack> getKitItems(Inventory inv) {
        List<ItemStack> kitItems = new ArrayList<>();

        int slot = 0;
        for (ItemStack item : inv.getInventory().getContents()) {
            if (item != null) {
                if (slot < 45) {
                    kitItems.add(item);
                }
            }
            slot++;
        }

        return kitItems;
    }

}
