package games.indigo.kits.guis;

import com.asylumdevs.inventoryapi.Inventory;
import com.asylumdevs.inventoryapi.InventoryAPI;
import com.asylumdevs.inventoryapi.ItemListener;
import games.indigo.kits.KitCategory;
import games.indigo.kits.Kits;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;

import java.util.Arrays;

public class KitCategoryGUI {

    private Kits kits = Kits.getInstance();

    public void open(Player player) {
        Inventory inv = InventoryAPI.createInventory(kits.getUtils().format("&6&lKit Categories"), InventoryType.CHEST);
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

        for (KitCategory kitCategory : kits.getKitCategories()) {
            inv.setSlot(kitCategory.getSlot(), InventoryAPI.createItem(kits.getUtils().format(kitCategory.getName()), kits.getUtils().formatArray(Arrays.asList("", "&7" + kitCategory.getDescription())), kitCategory.getIcon(), null, 1), new ItemListener() {
                @Override
                public void onItemClick(Player player, ClickType clickType) {
                    kits.getKitGUI().open(player, kitCategory);
                }
            });
        }

        inv.open(player);
    }

}
