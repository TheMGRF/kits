package games.indigo.kits.guis;

import com.asylumdevs.inventoryapi.Inventory;
import com.asylumdevs.inventoryapi.InventoryAPI;
import com.asylumdevs.inventoryapi.ItemListener;
import games.indigo.kits.Kit;
import games.indigo.kits.KitCategory;
import games.indigo.kits.Kits;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class KitGUI {

    private Kits main = Kits.getInstance();

    public void open(Player player, KitCategory kitCategory) {
        Inventory inv = InventoryAPI.createInventory(main.getUtils().format("&6&lKits Menu"), InventoryType.CHEST);
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

        int slot = 0;
        for (Kit kit : kitCategory.getKits()) {
            inv.setSlot(slot, kitItem(kit, player), new ItemListener() {
                @Override
                public void onItemClick(Player player, ClickType clickType) {
                    if (clickType.isLeftClick()) {
                        if (player.hasPermission("indigo.kits." + kit.getIdentifier())) {
                            kit.giveKit(player);
                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        }
                    } else if (clickType.isRightClick()) {
                        Inventory inv = InventoryAPI.createInventory1(main.getUtils().format("&6&lKit Preview &8&l> &8" + kit.getName()), 54);

                        int slot = 0;
                        for (ItemStack item : kit.getItems()) {
                            inv.setSlot(slot, item, new ItemListener() {
                                @Override
                                public void onItemClick(Player player, ClickType clickType) {
                                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 0.5f, 1);
                                }
                            });
                            slot++;
                        }

                        inv.setSlot(49, InventoryAPI.createItem("&7&l< &c&lBack", null, Material.BARRIER, null, 1), new ItemListener() {
                            @Override
                            public void onItemClick(Player player, ClickType clickType) {
                                main.getKitCategoryGUI().open(player);
                            }
                        });

                        inv.open(player);

                    }
                }
            });
            slot++;
        }

        inv.setSlot(22, InventoryAPI.createItem("&7&l< &c&lBack", null, Material.BARRIER, null, 1), new ItemListener() {
            @Override
            public void onItemClick(Player player, ClickType clickType) {
                main.getKitCategoryGUI().open(player);
            }
        });

        inv.open(player);
    }

    private ItemStack kitItem(Kit kit, Player player) {
        ItemStack item = InventoryAPI.createItem(main.getUtils().format("&e" + kit.getName()), main.getUtils().formatArray(Arrays.asList("", " &6&lSTATUS", (player.hasPermission("indigo.kits." + kit.getIdentifier()) ? "  &a&lUNLOCKED!" : "  &c&lLOCKED! &7Use &3&lCrystals &7to unlock ranks &7in &b/shop&7!"), "", " &6&lCOST", "  " + kit.getCost().getColours() + kit.getCost().getSymbol() + (kit.getCostValue() != 0 ? kit.getCostValue() : ""), "", " &6&lCOOLDOWN", "  &c" + main.getUtils().getFormattedTimeLeft(kit.getCooldown()), "", " &6&lUSAGE", "  &a&lLEFT-CLICK &7to claim kit!", "  &2&lRIGHT-CLICK &7to preview kit!")), kit.getIcon(), null, 1);
        ItemMeta meta = item.getItemMeta();

        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

        item.setItemMeta(meta);

        return item;
    }

}
