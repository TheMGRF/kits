package games.indigo.kits;

import org.bukkit.ChatColor;

import java.util.List;

public class Utils {

    /**
     * Format a string to contain Bukkit colour codes
     * @param msg The message to format
     * @return The formatted colour coded string
     */
    public String format(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    /**
     * Format an array to contain Bukkit colour codes
     * @param msgs The array to format
     * @return The formatted colour coded array
     */
    public List<String> formatArray(List<String> msgs) {
        for (int x = 0; x < msgs.size(); x++) {
            msgs.set(x, format(msgs.get(x)));
        }
        return msgs;
    }

    /**
     * Get seconds formatted as time
     * @param time The seconds in time
     * @return A formatted time string
     */
    public String getFormattedTimeLeft(int time) {
        int timeLeft = time;
        int hours = timeLeft / 3600;
        int minutes = (timeLeft % 3600) / 60;
        int seconds = timeLeft % 60;
        String timeString = String.format("%02dh %02dm %02ds", hours, minutes, seconds);
        return timeString;
    }


}
